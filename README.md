# BIM & Scan� Third-Party Library (moor)

![BIM & Scan](BimAndScan.png "BIM & Scan� Ltd.")

Conan build script for [moor](https://github.com/jlblancoc/nanoflann/), a C++ 'libarchive' wrapper.

Supports snapshot 22/7/2015 (unstable, from Git repo).

Requires a C++11 compiler (or higher).

Requires the [Bincrafters](https://bintray.com/bincrafters/public-conan) and [BIM & Scan� (public)](http://bsdev-jfrogartifactory.northeurope.cloudapp.azure.com/artifactory/webapp/) Conan repositories for third-party dependencies.
