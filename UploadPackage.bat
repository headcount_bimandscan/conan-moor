@ECHO OFF

REM
REM 2018-2019 © BIM & Scan® Ltd.
REM See 'LICENCE.md' in the project root for more information.
REM

CALL conan upload -c -r "bimandscan-public" --all "moor/20150722@bimandscan/unstable"

@ECHO ON
