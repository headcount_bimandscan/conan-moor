#!/bin/sh

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'LICENCE.md' in the project root for more information.
#

set -e
conan upload -c -r "bimandscan-public" --all "moor/20150722@bimandscan/unstable"
