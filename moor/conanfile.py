#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'LICENCE.md' in the project root for more information.
#
from conans import CMake, \
                   tools

from conans.model.conan_file import ConanFile
from conans.errors import ConanInvalidConfiguration


class Moor(ConanFile):
    name = "moor"
    version = "20150722"
    license = "MIT"
    url = "https://bitbucket.org/headcount_bimandscan/conan-moor"
    description = "A 'libarchive' C++ wrapper."
    author = "Neil Hyland <neil.hyland@bimandscan.com>"
    generators = "cmake"
    homepage = "https://github.com/saboorian/moor"

    _src_dir = "moor_src"

    settings = "os", \
               "compiler", \
               "build_type", \
               "arch"

    exports = "../LICENCE.md"

    options = {
                  "shared": [
                                True,
                                False
                            ],
                  "fPIC": [
                              True,
                              False
                          ]
              }

    default_options = "shared=False", \
                      "fPIC=True"

    requires = "libarchive/3.3.3@bimandscan/stable"

    def _valid_cppstd(self):
        return self.settings.compiler.cppstd == "17" or \
               self.settings.compiler.cppstd == "gnu17" or \
               self.settings.compiler.cppstd == "20" or \
               self.settings.compiler.cppstd == "gnu20" or \
               self.settings.compiler.cppstd == "11" or \
               self.settings.compiler.cppstd == "gnu11" or \
               self.settings.compiler.cppstd == "14" or \
               self.settings.compiler.cppstd == "gnu14"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

        if not self._valid_cppstd():
            raise ConanInvalidConfiguration("Library requires C++11 or higher!")

    def source(self):
        git_uri = "https://github.com/saboorian/moor.git"
        src_cmakefile = f"{self._src_dir}/CMakeLists.txt"
        src_cmakefile2 = f"{self._src_dir}/moor/CMakeLists.txt"

        git = tools.Git(folder = self._src_dir)
        git.clone(url = git_uri)
        git.checkout(element = "a7ea95722644a785528f3160255fbeb2426a0e39") # commit hash -> 22/7/2015

        # Patch CMake configuration:
        tools.replace_in_file(src_cmakefile,
                              "project(Moor CXX)",
                              "project(Moor CXX)\ninclude(\"${CMAKE_BINARY_DIR}/conanbuildinfo.cmake\")\nconan_basic_setup(TARGETS)\n")

        tools.replace_in_file(src_cmakefile,
                              "add_subdirectory(test)",
                              "#add_subdirectory(test)")

        # Only install static or shared libraries, never both:
        if self.options.shared:
            tools.replace_in_file(src_cmakefile2,
                                  "INSTALL(TARGETS moor moor_static",
                                  "install(TARGETS moor")
        else:
            tools.replace_in_file(src_cmakefile2,
                                  "INSTALL(TARGETS moor moor_static",
                                  "install(TARGETS moor_static")

    def configure_cmake(self):
        cmake = CMake(self)

        # Configure CMake library build:
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = self.package_folder

        if self.settings.os != "Windows":
            cmake.definitions["CMAKE_POSITION_INDEPENDENT_CODE"] = self.options.fPIC

        return cmake

    def build(self):
        cmake = self.configure_cmake()
        cmake.configure(source_folder = self._src_dir)
        cmake.build()

    def package(self):
        cmake = self.configure_cmake()
        cmake.install()

        self.copy("COPYING",
                  "licenses",
                  self._src_dir)

    def package_info(self):
        self.cpp_info.libdirs = [
                                    "lib"
                                ]

        self.cpp_info.includedirs = [
                                        "include"
                                    ]

        self.cpp_info.libs = tools.collect_libs(self)
