/*
 * 2018-2019 © BIM & Scan® Ltd.
 * See 'LICENCE.md' in the project root for more information.
 */
#include <cstdlib>
#include <iostream>

#include <moor/archive_writer.hpp>
#include <moor/archive_reader.hpp>


int main(int p_arg_count,
         char** p_arg_vector)
{
    std::cout << "'Moor' package test (compilation, linking, and execution).\n";

    moor::ArchiveWriter compressor(moor::Formats::Format_7Zip,
                                   moor::Compressions::Compression_none);

    compressor.SetFormatOption(moor::FORMAT_COMPRESSION_7ZIP,
                               moor::FORMAT_COMPRESSION_7ZIP_STORE);

    std::cout << "'Moor' package works!" << std::endl;
    return EXIT_SUCCESS;
}
